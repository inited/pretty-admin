interface URLTimestamp {
    url: string;
    timestamp: number;
}

interface FileUploadResponse {
    url: string;
}

