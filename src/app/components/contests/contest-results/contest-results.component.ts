import { Component, OnDestroy, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { Subscription } from "rxjs/Subscription";
import { ContestsService } from "../../../services/contests.service";
import { Utils } from "../../../Utils/utils";


@Component({
  selector: 'app-contest-results',
  templateUrl: './contest-results.component.html',
  styleUrls: ['./contest-results.component.css']
})
export class ContestResultsComponent implements OnInit, OnDestroy {
  public results: Array<any> = [];
  public contest: any = {};
  public answerCountsVoting = {};
  public answerCountsRating = [];
  public max: string;
  public loading: boolean;
  public sub: Subscription;
  public utils: Utils = new Utils();


  constructor(private route: ActivatedRoute, private contestsService: ContestsService, private router: Router) { }

  ngOnInit() {
    this.loading = true;
    this.sub = this.route.params.subscribe(params => {
      const id = +params['id']; // (+) converts string 'id' to a number
      this.getResults(id).then(() => {
        console.log("mam vysledek");
        this.processAnswers();
      });
    });
  }

  public async getResults(id: number) {
    this.loading = true;
    const contestPromise = this.contestsService.contestDetail(id);
    const resultsPromise = this.contestsService.getResults(id);
    let [contestResponse, resultsResponse] = [await contestPromise, await resultsPromise];
    if (contestResponse.status == "ok") {
      this.contest = contestResponse.data;
      console.log("contest", this.contest);
    } else {
      console.log(contestResponse.status);
      console.log("error", contestResponse);
    }
    if (resultsResponse.status == "ok") {
      this.results = resultsResponse.data;
      console.log("results", this.results);
    } else {
      console.log(resultsResponse.status);
      console.log("error", resultsResponse);
    }
    this.loading = false;
  }

  private processAnswers() {
    if (this.contest.contestType === 'voting') {
      this.contest.answers.forEach(item => {
        this.answerCountsVoting[item] = 0;
      });
      this.results.forEach(result => {
        result.answers.forEach(answer => {
          this.answerCountsVoting[answer]++;
        });
      });
    }
    if (this.contest.contestType === 'rating') {
      let i = 0;
      this.contest.answers.forEach(item => {
        this.answerCountsRating[i++] = 0;
      });
      this.results.forEach(result => {
        i = 0;
        result.answers.forEach(userAnswer => {
          this.answerCountsRating[i++] += parseInt(userAnswer, 10);
        });
      });
      console.log("answerCounts", this.answerCountsRating);
    }

    this.findMax(this.contest.contestType);
  }



  findMax(type: string) {

    let localMax = 0;
    let localMaxKey = "";
    if (type === 'voting') {
      this.contest.answers.forEach(it => {
        if (this.answerCountsVoting[it] > localMax) {
          localMaxKey = it;
          localMax = this.answerCountsVoting[it];
        }
      });
    } else if (type === 'rating') {
      this.answerCountsRating.forEach(it => {
        if (it > localMax) {
          localMaxKey = it;
          localMax = it;
        }
      });
    }
    this.max = localMaxKey;
  }

  public returnToOverview() {
    this.router.navigate(["contests"]);
  }

  public exportToCsv() {
    this.downloadData("results.csv", this.results);

  }

  private downloadData(filename: string, data: any) {
    let csv = this.utils.convertArrayOfObjectsToCSV({
      data: data
    });
    if (csv === null) return;
    if (!csv.match(/^data:text\/csv/i)) {
      csv = "data:text/csv;charset=utf-8," + csv;
    }
    const toDownload: any = encodeURI(csv);

    const link: any = document.createElement("a");
    link.setAttribute("href", toDownload);
    link.setAttribute("download", filename);
    link.click();
  }

  saveEmails() {
    const emails = [];
    this.results.forEach(it => {
      emails.push({email: it.email});
    });
    // let csv: any = this.utils.convertArrayOfObjectsToCSV({
    //   data: emails
    // });
    this.downloadData("emails.csv", emails);
  }

  public ngOnDestroy(): void {
    if (this.sub) {
      this.sub.unsubscribe();
    }
  }


}
