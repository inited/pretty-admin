/* tslint:disable:linebreak-style */
import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { Router } from "@angular/router";
import { BsModalRef, BsModalService } from "ngx-bootstrap";
import { isNullOrUndefined } from "util";
import { NotificationService } from "../../services/notification.service";
import { NotificationModel } from "./notification-model";

@Component({
    selector: "app-notifications",
    templateUrl: "./notifications.component.html",
    styleUrls: ["./notifications.component.css"]
})
export class NotificationsComponent implements OnInit {
    public notificationForm: FormGroup;
    public loading: boolean;
    public reload: boolean = false;
    public modalRef: BsModalRef;
    public errorMessage: string;
    public requiredError: boolean = false;

    constructor(
        private notificationService: NotificationService,
        private modalService: BsModalService,
        private router: Router) {
        this.notificationForm = this.createFormGroup();
    }

    public ngOnInit() {
    }

    private createFormGroup(): FormGroup {
        return new FormGroup({
            title: new FormControl(),
            description: new FormControl(),
            // test: new FormControl(true)
        });
    }

    public async onSubmit(templateOk: any, templateErr: any): Promise<void> {
        this.errorMessage = "";
        this.requiredError = false;
        this.reload = false;
        console.log(this.notificationForm);
        let notificationData: NotificationModel = Object.assign({}, this.notificationForm.value);
        notificationData.test = false;
        if (isNullOrUndefined(notificationData.title) || notificationData.title === "") {
            this.errorMessage = "Titulek je povinné pole.";
            this.requiredError = true;
            this.modalRef = this.modalService.show(templateErr);
            return;
        }
        this.loading = true;
        let response: any = await this.notificationService.sendNotification(notificationData);
        this.loading = false;

        if (response.status == "ok") {
            this.reload = true;
            console.log("ok");
            this.modalRef = this.modalService.show(templateOk);
        } else if (response.status == "unauthorized") {
            this.router.navigate(["login"]);
        } else {
            this.errorMessage = "Network error";
            this.modalRef = this.modalService.show(templateErr);
        }
    }

    public close(): void {
        this.modalRef.hide();
        this.notificationForm.reset(this.notificationForm.value);
    }

    public fillInDetails(notification: any): void {
        this.notificationForm.setValue({
            title: !! notification.title ? notification.title : "",
            description: !! notification.description ? notification.description : "",
            // test: !! notification.test ? notification.test : false
        });
    }
}
