import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WhyToBuyComponent } from './why-to-buy.component';

describe('WhyToBuyComponent', () => {
  let component: WhyToBuyComponent;
  let fixture: ComponentFixture<WhyToBuyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WhyToBuyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WhyToBuyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
