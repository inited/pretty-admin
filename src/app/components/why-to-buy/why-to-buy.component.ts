import { Component, OnInit } from "@angular/core";
import { ArticlesService } from "../../services/articles.service";

@Component({
    selector: "app-why-to-buy",
    templateUrl: "./why-to-buy.component.html",
    styleUrls: ["./why-to-buy.component.css"]
})
export class WhyToBuyComponent implements OnInit {
    public whyToBuy: string;
    public data: any;
    public loading: boolean = true;

    constructor(private articleService: ArticlesService) {
    }

    public ngOnInit() {
        this.getData();
    }

    private async getData() {
        let response: any = await this.articleService.getArticles();
        console.log(response);
        if (response.status == "ok") {
            this.data = response.data;
            this.whyToBuy = this.data.text;
            this.loading = false;
        } else {
            console.log(response.status);
            console.log("error", response);
            this.loading = false;
        }

    }

    public async onSubmit(templateOk, templateErr) {
        this.loading = true;
        this.data.text = this.whyToBuy;
        let response: any = await this.articleService.saveArticle(this.data);
        if (response.status == "ok") {
            this.loading = false;
        } else {
            console.log(response.status);
            console.log("error", response);
            this.loading = false;
        }
    }

}
