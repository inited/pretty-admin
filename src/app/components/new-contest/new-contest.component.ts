import { Component, OnDestroy, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { Storage } from "@ionic/storage";
import { BsModalRef, BsModalService } from "ngx-bootstrap";
import { isNullOrUndefined } from "util";
import { ContestsService } from "../../services/contests.service";

const GALLERY_PIC = 1;
const PICTURE = 2;

@Component({
    selector: "app-new-contest",
    templateUrl: "./new-contest.component.html",
    styleUrls: ["./new-contest.component.css"]
})
export class NewContestComponent implements OnInit, OnDestroy {

    public contest: any = {
        contestType: "rating",
        status: "paused",
        answers: [],
        photoAnswers: [],
        image: "",
        question: "",
        finishOn: new Date(new Date().getTime() + 7 * 24 * 60 * 60 * 1000) // za tyden konec
    };

    public min: Date = new Date();
    public answer: string;
    public dateTime: Date = this.contest.finishOn;

    public errors = {
        question: false,
        answers: false,
        photoAnswers: false
    };

    public allItems: any = {
        contests: [],
    };
    public modalRef: BsModalRef;
    public errorMessage: string = "";
    public loading: boolean = false;
    public new: boolean = true;
    public newId: any;
    public params: any;
    public photoGalleryToDelete: number;

    constructor(private route: ActivatedRoute, private router: Router, private contestsService: ContestsService,
                private storage: Storage, private modalService: BsModalService) {
    }

    public ngOnDestroy(): void {
        console.log("destroy");
    }

    public async ngOnInit(): Promise<void> {
        console.log(this.contest)
        await this.route.queryParams.subscribe((params: any) => {
            this.params = params;
            console.log(this.params);
        });
        if (this.params.isNew == 0) {
            await this.getContestDetail(this.params.id);
        }
    }

    public async getContestDetail(id: any): Promise<void> {
        this.loading = true;
        let response: any = await this.contestsService.contestDetail(id);
        this.loading = false;
        console.log("edit", response);
        if (response.status == "ok") {
            this.contest = response.data;
            this.dateTime = new Date(this.contest.finishOn * 1000);
            this.contest.photoAnswers = this.contest.answers;
            console.log(this.contest);
        } else if (response.status == "unauthorized") {
            await this.storage.set("user", undefined);
            this.router.navigate(["login"]);
        } else {
            console.log(response.status);
        }
    }

    private resetErrors() {
        this.errorMessage = "";
        this.errors["question"] = false;
        this.errors["photoAnswers"] = false;
        this.errors["answers"] = false;
    }

    private checkContest(templateErr: any): boolean {
        this.resetErrors();
        let isError = false;
        if (isNullOrUndefined(this.contest.question) || this.contest.question === "") {
            this.errors.question = true;
            isError = true;
        }

        if (this.contest.contestType === "voting" && this.contest.answers.length === 0) {
            this.errors.answers = true;
            isError = true;
        }

        if (this.contest.contestType === "rating" &&
            this.contest.answers.length === 0 && this.contest.photoAnswers.length === 0) {
            this.errors.photoAnswers = true;
            isError = true;
        }

        if (isError) {
            this.generateErrorMessage();
            this.modalRef = this.modalService.show(templateErr);
        }

        return !isError;
    }

    private mapErrorMessage(item: string): string {
        switch (item) {
            case "question":
                return "Soutěžní otázka";
            case "answers":
                return "Možné odpovědi";
            case "photoAnswers":
                return "Fotoodpovědi";
        }
    }

    private generateErrorMessage(){
        this.errorMessage = "Povinné položky nebyly vyplněny: ";
        let index = 0;
        Object.keys(this.errors).forEach(it => {
            if (this.errors[it]) {
                this.errorMessage += `'${this.mapErrorMessage(it)}'`;
                if (index !== 2) {
                    this.errorMessage += ", ";
                }
            }
            index++;
        });

    }

    public async save(templateErr: any, templateOk: any): Promise<void> {

        if (this.contest.contestType === "rating") {
          this.contest.answers = this.contest.photoAnswers;
        }

        if (!this.checkContest(templateErr)) {
            return;
        }



        let contestToSend: any = {
            finishOn: Math.floor(new Date(this.dateTime).getTime() / 1000),
            status: this.contest.status,
            contestType: this.contest.contestType,
            question: this.contest.question,
            image: this.contest.image,
            answers: this.contest.answers
        };

        if (this.params.isNew === "0") {
            contestToSend.id = this.contest.id;
        }

        this.loading = true;
        let response: any = await this.contestsService.saveContest(contestToSend, this.params.isNew === "0");
        this.loading = false;
        if (response.status == "ok") {
            console.log("ok");
            this.newId = response.data.id;
            this.modalRef = this.modalService.show(templateOk);
        } else if (response.status == "unauthorized") {
            await this.storage.set("user", undefined);
            this.router.navigate(["login"]);
        } else if (response.status == "notAll") {
            this.errorMessage = "Fill all fields.";
            this.modalRef = this.modalService.show(templateErr);
        } else {
            this.errorMessage = "Network error";
            this.modalRef = this.modalService.show(templateErr);
        }
    }

    public close(): void {
        this.modalRef.hide();
        this.router.navigate(["contests"]);
    }

    public delete(from: any, what: any): void {
        console.log(from, what);
        let index: number = from.indexOf(what);
        console.log(index);
        from.splice(index, 1);
        console.log(from);
        this.contest["" + from] = from;
    }

    public reset(input: any, file: any, name: any): void {
        input.value = "";
        file = undefined;
        name = "";
    }

    public async closeAndReload(): Promise<void> {
        this.modalRef.hide();
        await this.getContestDetail(this.newId);
    }

    public deleteProduct(template: any): void {
        this.modalRef = this.modalService.show(template);
    }

    public async deleteConfirmed(template: any): Promise<void> {
        this.modalRef.hide();
        this.loading = true;
        let response: any = await this.contestsService.deleteContest(this.contest.id);
        this.loading = false;
        console.log(response);
        if (response.status == "ok") {
            this.router.navigate(["contests"]);
        } else if (response.status == "unauthorized") {
            await this.storage.set("user", undefined);
            this.router.navigate(["login"]);
        } else {
            this.errorMessage = "Network error";
            this.modalRef = this.modalService.show(template);
        }
    }

    public openFile(file: any): void {
        window.open(file, "_blank");
    }


    public addPossibleAnswer() {
        if (this.answer) {
            this.contest.answers.push(this.answer);
            this.answer = '';
        }
    }

    public deleteAnswer(index: number) {
        this.contest.answers.splice(index, 1);
    }

    public isAnswerAvailable(): boolean {
        if (isNullOrUndefined(this.answer) || this.answer === "") {
            return false;
        }
        return true;
    }

    public photoAnswerChange(fileInput: any, tmp: any): void {
        const answer: any = fileInput.target.files[0];
        const answerName: string = answer.name;
        this.uploadFileImg(tmp, GALLERY_PIC, answer);
    }

    public photoChange(fileInput: any, tmp: any): void {
        const imgFile: any = fileInput.target.files[0];
        const imgName: string = imgFile.name;
        this.uploadFileImg(tmp, PICTURE, imgFile);
    }

    public async uploadFileImg(template: any, imageType: number, imgFile: File): Promise<void> {
        let response: any = await this.contestsService.uploadFile(imgFile);
        if (response.status == "ok") {
            if (imageType === PICTURE) {
                this.contest.image = response.data.url;
            } else {
                this.contest.photoAnswers.push(response.data.url);
            }
            console.log("ok");
        } else {
            this.errorMessage = "Network error";
            this.modalRef = this.modalService.show(template);
        }
    }

    public deletePhotoFromGallery(pic: number, template: any): void {
        this.photoGalleryToDelete = pic;
        this.modalRef = this.modalService.show(template);
    }

    public deletePhotoConfirmed(template: any): void {
        this.contest.photoAnswers.splice(this.photoGalleryToDelete, 1);
        this.modalRef.hide();
    }

    public resetFile(): void {
        this.contest.image = "";
    }
}
