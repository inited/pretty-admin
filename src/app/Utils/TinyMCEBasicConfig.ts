export class TinyMCEBasicConfig {
    public skin_url: string = "https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.7.6/skins/lightgray/";
    public menubar: boolean = false;
    public plugins: string = "link";
    public height: number = 250;
}
