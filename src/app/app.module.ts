import { registerLocaleData } from "@angular/common";
import { LOCALE_ID, NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";

import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpModule } from "@angular/http";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { RouterModule, Routes } from "@angular/router";
import { IonicStorageModule } from "@ionic/storage";
import { EditorModule } from "@tinymce/tinymce-angular";
import { OwlDateTimeIntl, OwlDateTimeModule, OwlNativeDateTimeModule } from "ng-pick-datetime";
import { ModalModule, TabsModule } from "ngx-bootstrap";
import { LoadingModule } from "ngx-loading";
import { AppComponent } from "./app.component";
import { NavbarComponent } from "./components/navbar/navbar.component";
import { NotificationsComponent } from "./components/notifications/notifications.component";
import { ArticlesService } from "./services/articles.service";
import { ContestsService } from "./services/contests.service";
import { NotificationService } from "./services/notification.service";

import "../assets/js/tinymce/tinymce";

import localeCs from "@angular/common/locales/cs";
import { ProgressHttpModule } from "angular-progress-http";
import "../assets/js/tinymce/plugins/image";
import "../assets/js/tinymce/plugins/imagetools";
import "../assets/js/tinymce/plugins/link";
import "../assets/js/tinymce/plugins/paste";
import "../assets/js/tinymce/themes/modern/theme";
import { ContestsComponent } from "./components/contests/contests.component";
import { NewContestComponent } from "./components/new-contest/new-contest.component";
import { NotificationHistoryComponent } from "./components/notifications/notification-history/notification-history.component";
import { WhyToBuyComponent } from "./components/why-to-buy/why-to-buy.component";
import { TruncatePipe } from "./pipes/truncate.pipe";
import { ContestResultsComponent } from "./components/contests/contest-results/contest-results.component";



const appRoutes: Routes = [
    {path: "", component: AppComponent},
    {path: "contests", component: ContestsComponent},
    {path: "newContest", component: NewContestComponent},
    {path: "notifications", component: NotificationsComponent},
    {path: "whyToBuy", component: WhyToBuyComponent},
    {path: "result/:id", component: ContestResultsComponent}
];

registerLocaleData(localeCs, "cz-CZ");
export class DefaultIntl extends OwlDateTimeIntl {
    /** A label for the cancel button */
    public cancelBtnLabel: string = "Zrušit";

    /** A label for the set button */
    public setBtnLabel: string = "Potvrdit";
}

export const url: string = "";
export const apiUrl: string = url + "/api";

@NgModule({
    declarations: [
        AppComponent,
        NavbarComponent,
        NewContestComponent,
        ContestsComponent,
        NotificationsComponent,
        TruncatePipe,
        NotificationHistoryComponent,
        WhyToBuyComponent,
        ContestResultsComponent
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        OwlDateTimeModule,
        OwlNativeDateTimeModule,
        HttpModule,
        FormsModule,
        ReactiveFormsModule,
        EditorModule,
        LoadingModule,
        TabsModule.forRoot(),
        IonicStorageModule.forRoot(),
        ModalModule.forRoot(),
        RouterModule.forRoot(appRoutes),
        ProgressHttpModule,
    ],
    providers: [
        {provide: OwlDateTimeIntl, useClass: DefaultIntl},
        ContestsService,
        ArticlesService,
        NotificationService,
        { provide: LOCALE_ID, useValue: "cz-CZ" }
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
