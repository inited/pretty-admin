import { TestBed, inject } from '@angular/core/testing';

import { ContestsService } from './contests.service';

describe('ContestsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ContestsService]
    });
  });

  it('should be created', inject([ContestsService], (service: ContestsService) => {
    expect(service).toBeTruthy();
  }));
});
