import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { Storage } from "@ionic/storage";

@Component({
    selector: "app-root",
    templateUrl: "./app.component.html",
    styleUrls: ["./app.component.css"]
})
export class AppComponent {
    public title: string = "app";

    constructor(private storage: Storage, private router: Router) {
        this.router.navigate(["contests"]);
    }
}
